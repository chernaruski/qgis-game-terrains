exports.execute = async (args) => {
    // args => https://egodigital.github.io/vscode-powertools/api/interfaces/_contracts_.filechangeeventactionscriptarguments.html

    const path = require('path');

    // s. https://code.visualstudio.com/api/references/vscode-api
    const vscode = args.require('vscode');

    vscode.window.showInformationMessage(
        `The following file has changed: ${ 
            path.relative(
                __dirname + '/..',
                args.file.fsPath
            )
         }`
    );
};