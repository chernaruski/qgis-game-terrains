[ ] 1.0 release
    [X] Move map extent layer to project location
    [ ] Remember state of widget (And open by default)

[ ] QGIS improvements
    [ ] use QgsTask.setDependentLayers
    [ ] Allow cancelling QgsTasks from statusbar
    [ ] Add spacebar map panning when Mark is active

## Heightmap
[ ] Give good error when no SRTM available for marked area
[ ] Proper Network error reporting

TB BMP import is from 0 to 4000
http://published-files.eea.europa.eu/eudem/eea_r_3035_25_m_gsgrda-eudem-clshaded-europe_2012_rev1/tiles/EUD_CP-color-shaded_2500025000-AA-masked.tif


* [ ] Add the transformation coordinates to gtt_export.txt