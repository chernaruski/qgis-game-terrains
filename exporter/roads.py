import os
from PyQt5.QtCore import QVariant

from qgis.utils import iface
from qgis.core import Qgis, QgsGeometry
from qgis.core import QgsField, QgsFields, QgsVectorFileWriter, QgsCoordinateReferenceSystem, QgsFeature
from PyQt5.QtGui import QTransform

from ..functions import get_layer_from_name, get_save_location, create_transform_layer, message_log
from ..extent import transform_extent
from ..engine.arma import EngineArma


def export_roads(extent_crs="EPSG:32631", parent=None):
    layer = get_layer_from_name('osm lines')
    if layer is None:
        iface.messageBar().pushCritical("Missing Layer", "No layer called 'osm lines' found for roads export")
        message_log("To export roads, please use the download option first", level=Qgis.Warning)
        return None
    features = layer.getFeatures()

    featurearray = [feature for feature in features]
    if len(featurearray) == 0:
        return 0

    wkbtype = featurearray[0].geometry().wkbType()
    exportpath = str(get_save_location() / 'roads.shp')

    target_crs = QgsCoordinateReferenceSystem(extent_crs)
    export_crs = QgsCoordinateReferenceSystem(EngineArma.CRS)
    fields = QgsFields()
    fields.append(QgsField('ID', QVariant.Int))
    fields.append(QgsField('ORDER', QVariant.Int))
    fields.append(QgsField('__LAYER', QVariant.String))
    writer = QgsVectorFileWriter(exportpath, "utf-8", fields, wkbtype, export_crs, "ESRI Shapefile")

    transform = create_transform_layer(layer, target_crs=extent_crs)
    left, bottom, _, _ = transform_extent(targetcrs=extent_crs)
    extent_translate = (EngineArma.COORD_X - left, EngineArma.COORD_Y - bottom * -1)

    scale = iface.gtt.settings['scaled_size'] / iface.gtt.settings['mapsize']
    transform_scale = QTransform()
    transform_scale.scale(scale, scale)
    transform_scale.translate(EngineArma.COORD_X, EngineArma.COORD_Y)

    message_log(f"Translate road coords by: {extent_translate}")

    # Copy the roads we want
    for feature in layer.getFeatures():
        if feature['ID'] > 0:
            geom = feature.geometry()  # type: QgsGeometry
            geom.transform(transform)
            geom.translate(extent_translate[0], extent_translate[1], 0)  # Set them to arma's mapframe

            if scale != 1.0:
                geom.transform(transform_scale)

            featurenew = QgsFeature()
            featurenew.setGeometry(geom)
            featurenew.setAttributes([feature['ID'], feature['ORDER'], feature['GTT_road']])
            writer.addFeature(featurenew)

    del(writer)
