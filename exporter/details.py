"""Handles creating a details.txt file, containing info on export"""

from pathlib import Path

from qgis.core import QgsPoint
from gameterraintools import gtt
from gameterraintools.functions import get_save_location, create_transform_layer, message_log
from gameterraintools.extent import get_working_layer, wgs_to_utm
from gameterraintools.classification.roi.surfaces import readable_list

def export_details(settings):

    message_log("Writing export details file")

    path = get_save_location() / "gtt_export.txt"  # type: Path
    path.parent.mkdir(parents=True, exist_ok=True)
    alt_export = False  # Non-extent mode, uses imported data without any known extent

    extent_layer = get_working_layer()
    if extent_layer is not None:
        transform = create_transform_layer(extent_layer, "EPSG:4236")
        center = extent_layer.extent().center()
        center = QgsPoint(center.x(), center.y())
        center.transform(transform)
        middle_point = (center.y(), center.x())
    else:
        alt_export = True

    with path.open(mode="w") as file:
        if not alt_export:
            write_settings(file, settings)
            write_config_entries(file, middle_point, settings)
        write_surface_details(file)

def write_settings(file, settings):
    """Writes general export settings"""
    file.write("## Export settings ##\n")
    mapsize = settings['mapsize']


    scaled = settings['scaled']
    file.write(f"Scaling enabled = {scaled}\n")
    file.write(f"Tiling enabled = {settings['tilesize_enabled']}\n")
    if settings['tilesize_enabled']:
        file.write(f"Tile resolution = {settings['tilesize']}\n")
    file.write(f"Real terrain size (m) = {mapsize}\n")
    if scaled:
        mapsize = settings['scaled_size']

    resolution = settings['map_reso']
    if resolution == -1:
        resolution = mapsize

    heightmap_reso = settings['height_reso']
    file.write(f"\n## Terrain Sampler ##\n")
    file.write(f"Grid size = {heightmap_reso} x {heightmap_reso}\n")
    file.write(f"Cell size (m) = {mapsize / heightmap_reso}\n")
    file.write(f"Terrain size (m) = {mapsize}\n")
    file.write(f"Satmap size (px) = {resolution} x {resolution}\n")
    file.write(f"Resolution (m/px) = {mapsize / resolution}\n")

def write_config_entries(file, middle_point, settings):
    """Writes config entries used in arma config.cpp"""
    utm = wgs_to_utm(*middle_point, authid=False)
    file.write("\n## Arma config entries ##\n")
    file.write(f"longitude = {middle_point[1]};\n")
    file.write(f"latitude = {middle_point[0] * -1};\n")  # Latitude is reversed in arma (lol)
    file.write(f"mapSize = {settings['mapsize']};\n")
    file.write(f"mapZone = {utm};\n")

def write_surface_details(file):
    file.write("\n## Surface entries ##\n")
    for line in readable_list():
        file.write(line+"\n")

def write_coordinates(file):
    file.write("\n## Coordinates ##\n")