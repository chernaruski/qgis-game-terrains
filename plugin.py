from PyQt5.QtCore import Qt, QSettings, QCoreApplication, QTranslator, qVersion
from PyQt5.QtWidgets import QToolBar, QAction
from PyQt5.QtGui import QIcon

from .functions import plugin_path
from .gtt import GttTools
from .ui.gtt_widget import GameTerrainToolsDockWidget
from .objects.ui import objects_widget

class GameTerrainTools():
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        iface.gtt_plugin = self
        self.canvas = iface.mapCanvas()
        self.pluginIsActive = False
        self.iface = iface
        self.widgets = {}

        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = plugin_path / 'i18n' / 'GameTerrainTools_{}.qm'.format(locale)

        if locale_path.exists():
            self.translator = QTranslator()
            self.translator.load(str(locale_path))

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Game Terrain Tools')

        child = iface.mainWindow().findChild(QToolBar, 'GameTerrainTools')
        if child is None:
            self.toolbar = self.iface.addToolBar("Game Terrain Tools")
            self.toolbar.setObjectName("GameTerrainTools")
        else:
            self.toolbar = child
        self.tools = GttTools()

    def tr(self, message):
        """Get the translation for a string using Qt translation API."""
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('GameTerrainTools', message)

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        self.add_action(
            'icon.png',
            text=self.tr(u'Game Terrains'),
            callback=self.run,
            parent=self.iface.mainWindow())

        self.add_action(
            'icon.png',
            text=self.tr(u'Objects'),
            callback=objects_widget.create_widget,
            parent=self.iface.mainWindow())

    #--------------------------------------------------------------------------
    def widget(self, tag):
        try:
            return self.widgets[tag]
        except KeyError:
            return None

    def widgetCreate(self, tag, widget_class, **kwargs):
        if tag in self.widgets.keys():
            return None
        else:
            widget = widget_class(parent=self.iface.mainWindow(), **kwargs)
            widget.closeWidget.connect(self.widgetClose)
            self.iface.addDockWidget(Qt.RightDockWidgetArea, widget)
            self.widgets[tag] = widget
            widget.show()

    def widgetClose(self, tag):
        widget = self.widgets[tag]
        widget.deleteLater()
        self.iface.removeDockWidget(widget)
        del self.widgets[tag]

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""

        widgets = [self.widgets[key] for key in self.widgets.keys()]
        for widget in widgets:
            widget.close()

        self.tools.unload()
        for action in self.actions:
            self.iface.removePluginMenu(
                self.menu,
                action)
            
            self.iface.removeToolBarIcon(action)

        # remove the toolbar
        del self.toolbar
        del self.tools

    #--------------------------------------------------------------------------
    def run(self):
        """Run method that loads and starts the plugin"""

        self.widgetCreate('primary', GameTerrainToolsDockWidget, tools=self.tools)

    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(str(plugin_path / 'icons' / icon_path))
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)
        return action
