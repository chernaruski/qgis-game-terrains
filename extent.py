from math import floor
from qgis.core import QgsPoint, Qgis

from .functions import get_layer_from_name, move_layer_to_top, create_transform_layer, message_log
from .engine.arma import EngineArma
EXTENT = 'game terrain tools - map extent'


class NoMarkedAreaLayer(Exception):
    """Marked area layer doesn't exist"""
    def __init__(self, *args, **kwargs):
        default_message = 'This action requires you to mark target area first'
        message_log(default_message, level=Qgis.Critical)
        if args or kwargs:
            super().__init__(*args, **kwargs)
        else:
            super().__init__(default_message)


def get_working_layer():
    """Gets the current working layer"""
    return get_layer_from_name(EXTENT)

def raise_extent():
    """Raises the extent layer to the top"""
    extent = get_working_layer()
    if extent is not None:
        move_layer_to_top(extent)

def transform_extent(targetcrs=EngineArma.CRS) -> list:
    extent_layer = get_working_layer()
    extent = extent_layer.extent()
    if extent_layer.crs().authid() != targetcrs:
        transform = create_transform_layer(extent_layer, target_crs=targetcrs)
        bottom_left = QgsPoint(extent.xMinimum(), extent.yMinimum())
        top_right = QgsPoint(extent.xMaximum(), extent.yMaximum())
        bottom_left.transform(transform)
        top_right.transform(transform)

        return [bottom_left.x(), bottom_left.y(), top_right.x(), top_right.y()]
    return [extent.xMinimum(), extent.yMinimum(), extent.xMaximum(), extent.yMaximum()]

def extent_utm_zone(authid=True):
    """Gets the UTM zone for the middle of the marker area"""
    extent_layer = get_working_layer()
    if extent_layer is None:
        return EngineArma.CRS
    
    center = extent_layer.extent().center()
    if extent_layer.crs().authid() != "EPSG:4326":
        transform = create_transform_layer(extent_layer, target_crs="EPSG:4326")
        center = QgsPoint(center.x(), center.y())
        center.transform(transform)
    return wgs_to_utm(center.y(), center.x(), authid=authid)

def wgs_to_utm(lat, lon, authid=False):
    """Converts EPSG:4326 coordinates into UTM zone or authid number"""
    utm_zone = floor((lon + 180) / 6) + 1

    if (lat >= 56.0 and lat < 64.0 and lon >= 3.0 and lon < 12.0):
        utm_zone = 32
    
    if (lat >= 72.0 and lat < 84.0):
        if (lon >= 0.0 and lon <  9.0):
            utm_zone = 31
        elif (lon >= 9.0 and lon < 21.0):
            utm_zone = 33
        elif (lon >= 21.0 and lon < 33.0):
            utm_zone = 35
        elif (lon >= 33.0 and lon < 42.0):
            utm_zone = 37
 
    south = lat < 0
    name = f"UTM {utm_zone}{('N', 'S')[south]}"

    if authid:
        authid_ = 32600 + utm_zone
        if south:
            authid_ += 100
        return f"EPSG:{authid_}"
    return utm_zone
