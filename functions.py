from sys import executable
from tempfile import gettempdir
from pathlib import Path
from math import log as math_log

from PyQt5.QtCore import QFileInfo, QObject, Qt, QVariant, pyqtSignal
from PyQt5.QtWidgets import QProgressBar

from qgis import core
from qgis.core import (QgsApplication, QgsCoordinateReferenceSystem,
                       QgsCoordinateTransform, QgsCoordinateTransformContext,
                       QgsMapLayer, QgsProject, QgsVectorLayer)
from qgis.gui import QgsMessageBarItem
from qgis.utils import iface
from qgis.core import Qgis, QgsMessageLog, QgsSettings
from .static import SETTINGSNAME

class Logger(QObject):
    message = pyqtSignal(str)

    def __init__(self, message_log, category="Messages", parent=None):
        super().__init__(parent=parent)
        self.category = category
        self.logger = message_log

    def log(self, message, level=Qgis.Info, emit=True):
        """
            Writes a message to the QgsMessageLog

            Parameters:
            message (int): Message to write
            level (Qgis.Info): Level of message to write
        """
        print(message)
        self.logger.logMessage(str(message), tag=self.category, level=level)
        if emit:
            self.message.emit(str(message))

def load_temp_path() -> Path:
    settings = QgsSettings()
    path = settings.value(f"{SETTINGSNAME}/temp_path", "", type=str)
    if path == "":
        temp_path = Path(gettempdir()) / 'GameTerrainTools'
        temp_path.mkdir(parents=True, exist_ok=True)
        settings.setValue(f"{SETTINGSNAME}/temp_path", str(temp_path))
    else:
        temp_path = Path(path)
    return temp_path

temp_path = load_temp_path()
plugin_path = Path(__file__).parent

def get_temp_path() -> Path:
    settings = QgsSettings()
    return Path(settings.value(f"{SETTINGSNAME}/temp_path", gettempdir()))

logger = Logger(QgsMessageLog, 'GameTerrainTools')
def message_log(message: str, level=Qgis.Info, emit=True):
    logger.log(message, level, emit)


def add_file_logging(filename: str='gtt.log'):
    """Adds logging to given file from all messages in QgsMessageLog"""
    filepath = settings_path() / filename
    filepath.parent.mkdir(parents=True, exist_ok=True)
    filepath = str(filepath)
    def write_log_message(message, tag, level):
        with open(filepath, 'a') as logfile:
            logfile.write(f'{tag}({level}): {message}\n')
    QgsApplication.messageLog().messageReceived.connect(write_log_message)


def get_layer_from_name(name: str) -> QgsMapLayer:
    """gets qqgsVectorLayer from string"""
    layers = QgsProject.instance().mapLayersByName(name)
    if len(layers) == 0:
        return None
    return layers[0]


def get_layer_from_source(source) -> QgsMapLayer:
    """Gets layer from source path, used to circumvent processing not giving the return layer, but source"""
    layers = QgsProject.instance().mapLayers()
    for _key, layer in layers.items():
        if layer.source() == source:
            return layer
    return None


def profile_path() -> Path:
    """Gets profile path"""
    return Path(QgsApplication.instance().qgisSettingsDirPath())


def settings_path() -> Path:
    """Gets profile>settings path for the plugin"""
    return profile_path() / 'gameterraintools'  # type: Path


def get_save_location() -> Path:
    """Gets save location"""
    return project_folder() / "gtt_export"  # type: Path


def project_folder() -> Path:
    """Gets path to saved project folder"""
    projectfile = QFileInfo(QgsProject.instance().fileName())
    return Path(projectfile.absolutePath())


def qgis_bin_folder() -> Path:
    """Gets the bin folder"""
    return Path(executable).parent


def qgis_python_exe() -> Path:
    """Gets the python exe location"""
    return qgis_bin_folder().parent / 'apps' / 'Python37' / 'python.exe'


def move_layer_to_top(layername: str):
    """Moves given layer name to top"""
    if isinstance(layername, str):
        layer = get_layer_from_name(layername)
    else:
        layer = layername

    layerid = layer.id()
    if layer is not None:
        layertree = QgsProject.instance().layerTreeRoot()
        myalayer = layertree.findLayer(layerid)
        clone = myalayer.clone()
        parent = myalayer.parent()
        parent.insertChildNode(0, clone)
        parent.removeChildNode(myalayer)

        # order = layertree.customLayerOrder()
        # order.pop(order.index(layer))
        # order.insert(0, layer)
        # layertree.setHasCustomLayerOrder(True)
        # layertree.setCustomLayerOrder(order)
        QgsApplication.processEvents()


def set_layer_visibility(layer: QgsMapLayer, show=False):
    """Sets the layer visibility
    
    Args:
        layer (QgsMapLayer): Layer to change vis for
        show (bool, optional): Show or hide. Defaults to False.
    """
    QgsProject.instance().layerTreeRoot().findLayer(layer.id()).setItemVisibilityChecked(show)


def create_transform_layer(layer: QgsVectorLayer, target_crs='EPSG:4326') -> QgsCoordinateTransform:
    layer_crs = layer.crs()
    layer_crs_id = QgsCoordinateReferenceSystem(target_crs)
    return QgsCoordinateTransform(layer_crs, layer_crs_id, QgsProject.instance())


def create_transform(source_crs, target_crs='EPSG:4326'):
    if isinstance(source_crs, str):
        source_crs = QgsCoordinateReferenceSystem(source_crs)
    if isinstance(target_crs, str):
        target_crs = QgsCoordinateReferenceSystem(target_crs)
    return QgsCoordinateTransform(source_crs, target_crs, QgsProject.instance())


def get_attribute_column(sourcelayer: QgsVectorLayer, colummname: str, typename=QVariant.Int) -> int:
    """Gets a column for a feature, if none exists yet with given name, create one with the given type"""
    columnindex = sourcelayer.fields().lookupField(colummname)
    if columnindex == -1:
        _res = sourcelayer.addAttribute(core.QgsField(colummname, typename))
        sourcelayer.updateFields()
        columnindex = sourcelayer.fields().lookupField(colummname)
    return columnindex


def apply_stylefile(layer: QgsMapLayer, stylename: str):
    """Loads a layer style file"""
    stylefile = str(plugin_path / "resources_a" / (stylename + ".qml"))
    layer.loadNamedStyle(stylefile)


def show_progress(text="satellite", range_=(0, 0)) -> QgsMessageBarItem:
    progressMessageBar = iface.messageBar().createMessage(text)  # type: QgsMessageBarItem
    progressBar = QProgressBar()
    progressBar.setAlignment(Qt.AlignLeft|Qt.AlignVCenter)
    progressMessageBar.layout().addWidget(progressBar)
    iface.messageBar().pushWidget(progressMessageBar, core.Qgis.Info)
    progressBar.setRange(*range_)
    progressMessageBar.bar = progressBar
    return progressMessageBar

def handle_temp_name(path: Path) -> Path:
    """
        Handles splitting up the temporary uid, used in case files might be in use but we want 
        to make sure they get saved regardless. Returns new path if succesfull rename,
        else it'll return the original name
    """

    name = path.stem.split(".")[0]
    newpath = Path(path.parent / f"{name}{path.suffix}")
    try:
        path.replace(newpath)
        return newpath
    except PermissionError:
        return path

def get_zoom_level() -> int:
    """Gets zoom level in current canvas used for WMS tile downloading"""
    scale = iface.mapCanvas().scale()
    dpi = iface.mainWindow().physicalDpiX()
    maxScalePerPixel = 156543.04
    inchesPerMeter = 39.37
    zoomlevel = int(round(math_log(((dpi * inchesPerMeter * maxScalePerPixel) / scale), 2), 0))
    return zoomlevel