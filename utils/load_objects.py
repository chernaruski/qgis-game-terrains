"""
    Loads in an Arma TB-style txt file and makes shapes for it
"""

### PASTE THE PATH IN THERE (SHIFT-RIGHT CLICK IN WINDOW EXPLORER, THEN DO COPY AS PATH)
### keep the r" leading!
OBJECTPATH = r"B:\Projects\temp\nCCqxwQ3.txt"

from pathlib import Path
from PyQt5.QtCore import QVariant
from qgis.utils import iface
from qgis.core import QgsVectorLayer, QgsFeature, QgsFields, QgsField
from qgis.core import QgsCoordinateReferenceSystem, QgsVectorFileWriter, QgsWkbTypes
from qgis.core import QgsGeometry, QgsPointXY

def create_object_writer(path: Path, crs_id: str, fields: QgsFields):
    target_crs = QgsCoordinateReferenceSystem(crs_id)
    writer = QgsVectorFileWriter(str(path), "utf-8", fields, QgsWkbTypes.Point, target_crs, "ESRI Shapefile")
    return writer


def create_layer(object_path: Path):
    path = object_path.parent / 'objects2.shp'
    utm = 'EPSG:32631'
    fields = get_fields()
    writer = create_object_writer(path, utm, fields)
    objects = []

    i = 0

    for line in load_file(object_path):
        data = from_tb_format(line)  # type: dict

        x = data['position'][0]
        y = data['position'][1]
        z = data['position'][2]

        feature = QgsFeature(fields, i)
        feature.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(x, y)))
        feature.setAttributes([i, x, y, z, data['model'], data['scale']])
        writer.addFeature(feature)
        i += 1

    iface.addVectorLayer(str(path), 'gtt_', "ogr")
    del writer

def get_fields():
    fields = QgsFields()
    fields.append(QgsField('ID', QVariant.Int))
    fields.append(QgsField('X', QVariant.Double))
    fields.append(QgsField('Y', QVariant.Double))
    fields.append(QgsField('Z', QVariant.Double))
    fields.append(QgsField('MODEL', QVariant.String))
    fields.append(QgsField('SCALE', QVariant.Double))
    return fields

def from_tb_format(line: str) -> dict:
    """Constructs a tb object from a import/export line"""
    data = line.strip('\n').split(";")
    model = data[0]

    datafloat = [float(f) for f in data[1:8]]
    datafloat.insert(0, data[0])
    position = [datafloat[1], datafloat[2], datafloat[7]]
    rotation = [datafloat[4], datafloat[5], datafloat[3]]
    items = {
        "position": position, 
        "rotation": rotation, 
        "model": data[0],
        "scale": datafloat[6]
    }
    return items

def load_file(path: Path):
    """Loads in an object file"""
    with path.open(mode='r') as fp:
        for line in fp:
            yield line

if __name__ == "__console__":
    print("Running")
    object_path = Path(OBJECTPATH)
    create_layer(object_path)