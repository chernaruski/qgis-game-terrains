Open up the QGIS Project


## Open up Python console

![Console](readme\pic1.png)

## Open up the script
![Console](readme\pic2.png)

## Select the load objects
![Console](readme\pic3.png)

Set the OBJECTPATH = r"<path>"
Press the green start icon

## Open up Print Layout `test`
![Console](readme\pic4.png)

## Export as Image
![Console](readme\pic5.png)

## Set Resolution (after selecting save path)
![Console](readme\pic6.png)