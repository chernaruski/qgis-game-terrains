import math
import os
from pathlib import Path

from gameterraintools import gtt
from gameterraintools.functions import get_temp_path
from gameterraintools.extent import transform_extent

def srtm_tilenames(bounds):
    """
        https://github.com/bopen/elevation/blob/master/elevation/datasource.py
    """

    left = int(math.floor(bounds[0]))
    bottom = int(math.floor(bounds[1]))
    right = int(math.ceil(bounds[2]))
    top = int(math.ceil(bounds[3]))

    lat_diff = abs(top - bottom)
    lon_diff = abs(right - bottom)
    n_tiles = lat_diff * lon_diff
    image_counter = 0
    tiles = []
    
    for lat in range(bottom, top):
        for lon in range(left, right):
            if lon < 10 and lon >= 0:
                lon_tx = "E00%s" % lon
            elif lon >= 10 and lon < 100:
                lon_tx = "E0%s" % lon
            elif lon >= 100:
                lon_tx = "E%s" % lon
            elif lon > -10 and lon < 0:
                lon_tx = "W00%s" % abs(lon)
            elif lon <= -10 and lon > -100:
                lon_tx = "W0%s" % abs(lon)
            elif lon <= -100:
                lon_tx = "W%s" % abs(lon)

            if lat < 10 and lat >= 0:
                lat_tx = "N0%s" % lat
            elif lat >= 10 and lat < 100:
                lat_tx = "N%s" % lat
            elif lat > -10 and lat < 0:
                lat_tx = "S0%s" % abs(lat)
            elif lat < -10 and lat > -100:
                lat_tx = "S%s" % abs(lat)
            
            tile = f"{lat_tx}{lon_tx}.SRTMGL1.hgt.zip"
            tiles.append(tile)

    return (tiles, n_tiles, image_counter)

def srtm_download(downloader):
    bounds = transform_extent(targetcrs="EPSG:4326")
    tiles, n_tiles, image_counter = srtm_tilenames(bounds)

    if tiles:
        for tilename in tiles:
            baseurl = "https://e4ftl01.cr.usgs.gov//MODV6_Dal_D/SRTM/SRTMGL1.003/2000.02.11/"
            url = f"{baseurl}{tilename}"
            folder = get_temp_path() / "SRTM"
            target_file = folder / tilename
            if not folder.exists():
                folder.mkdir(parents=True, exist_ok=True)

            # Skip download if the file already exists
            if not Path(target_file).exists():
                credentials = ("nasa", "https://urs.earthdata.nasa.gov", "https://urs.earthdata.nasa.gov//users/new")
                def callback(filename: str, path: str):  # Get the name for the unpacked file
                    filepath = Path(filename)
                    return str((filepath.parent / filepath.name.split(".")[0]).with_suffix(".hgt"))

                downloader.get_file(url, target_file, credentials=credentials, cleanup=True, callback=callback)
            else:
                downloader.done.emit(str(target_file).replace("SRTMGL1.", "").replace(".zip", ""))
        return True
    else:
        return False
