from . import srtm
from . import eudem

sources = {
    "srtm": {
        "download": srtm.srtm_download
    },
    "eudem": {
        "download": eudem.eudem_download
    },
}