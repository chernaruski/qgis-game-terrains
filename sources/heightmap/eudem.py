"""
    Create tilename for download from:
    This is the 2012rev1 version, which is NOT the latest copernicus version,
    however those files get unique download links
    
    * https://data.europa.eu/euodp/data/dataset/data_eu-dem
    * https://www.eea.europa.eu/data-and-maps/data/eu-dem#tab-original-data

    Resulting name something like: 
    
    * http://published-files.eea.europa.eu/eudem/entr_r_4258_1_arcsec_gsgrda-eudem-dem-europe_2012_rev1/eudem_tiles_5deg/eudem_dem_5deg_n50e015.tif
"""

import math
import os
from pathlib import Path
from gameterraintools import gtt
from gameterraintools.functions import get_temp_path
from gameterraintools.sources.heightmap import eudem_available
from gameterraintools.extent import transform_extent

def eudem_tilename_5deg(bounds):
    """Creates the filename"""

    left = int(math.floor(bounds[0]))
    bottom = int(math.floor(bounds[1]))
    right = int(math.ceil(bounds[2]))
    top = int(math.ceil(bounds[3]))

    lat_diff = abs(top - bottom)
    lon_diff = abs(right - bottom)
    tiles = []
    
    TILE_SIZE = 5
    def _floor(number, mod=TILE_SIZE):
        return number - (number % mod)

    def _ceil(number, mod=TILE_SIZE):
        return number - (number % mod) + mod

    for lat in range(_floor(bottom), _ceil(top), TILE_SIZE):
        for lon in range(_floor(left), _ceil(right), TILE_SIZE):
            lat_tx = _floor(lat)
            lon_tx = _floor(lon)

            if lat_tx < 0:
                lat_letter = "s"
            else:
                lat_letter = "n"
            if lon_tx < 0:
                lon_letter = "w"
            else:
                lon_letter = "e"

            tilename = f"{lat_letter}{lat_tx:02}{lon_letter}{lon_tx:03}"
            print(tilename, lat, lon)
            if tilename in eudem_available.AVAILABLE_TILES:
                tile = f"eudem_dem_5deg_{tilename}.tif"
                if tile not in tiles:
                    tiles.append(tile)

    return tiles

def eudem_download(downloader):
    bounds = transform_extent(targetcrs="EPSG:4326")
    tiles = eudem_tilename_5deg(bounds)

    if tiles:
        for tilename in tiles:
            baseurl = "http://published-files.eea.europa.eu/eudem/entr_r_4258_1_arcsec_gsgrda-eudem-dem-europe_2012_rev1/eudem_tiles_5deg/"
            url = f"{baseurl}{tilename}"
            folder = get_temp_path() / "EUDEM"
            target_file = folder / tilename
            if not folder.exists():
                folder.mkdir(parents=True, exist_ok=True)

            # Skip download if the file already exists
            if not Path(target_file).exists():
                credentials = ("eea", "http://published-files.eea.europa.eu", "https://www.eea.europa.eu/themes/login_form")
                downloader.get_file(url, target_file, credentials=credentials, cleanup=True)
            else:
                downloader.done.emit(str(target_file))
        return True
    else:
        return False
