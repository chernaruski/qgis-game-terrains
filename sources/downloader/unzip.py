from zipfile import is_zipfile, ZipFile
from pathlib import Path
from qgis.utils import iface
from qgis.core import Qgis

from gameterraintools.functions import message_log

def unzip(zip_file: str, location: Path=None, cleanup: bool=True):
    """Unzips file to given location
    
    Args:
        zip_file (str): Path to zipfile
        location (str, optional): Location to unzip to, if not given will unpack in place. Defaults to "".
    """

    if not is_zipfile(zip_file):
        return zip_file

    try:
        if location is None:
            location = Path(zip_file).parent

        print(zip_file, location, cleanup)
        with ZipFile(zip_file, 'r') as zip_obj:
            zip_obj.extractall(str(location))

        if cleanup:
            Path(zip_file).unlink()
            
        return str(location)

    except Exception as e:
        message_log(f"Unpack error: {e}", level=Qgis.Critical)
        message_log(f"Path: {location.resolve()}", level=Qgis.Critical)
        return None

