# -*- coding: utf-8 -*-
#"""
#/***************************************************************************
# SrtmDownloader
#                                 A QGIS plugin
# Downloads SRTM Tiles from NASA Server
#                              -------------------
#        begin                : 2017-12-30
#        git sha              : $Format:%H$
#        copyright            : (C) 2017 by Dr. Horst Duester / Sourcepole AG
#        email                : horst.duester@sourcepole.ch
# ***************************************************************************/
#
#/***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************/
#"""
import os
import time
import zipfile
from pathlib import Path
from types import SimpleNamespace

from qgis.utils import iface
from qgis.core import Qgis
from PyQt5.QtCore import QUrl, Qt, QFileInfo,  QSettings, pyqtSignal, QObject
from PyQt5.QtWidgets import QApplication
from PyQt5.QtNetwork import QNetworkRequest, QNetworkReply,  QNetworkAccessManager

from ... import gtt
from ...functions import message_log
from ...static import SETTINGSNAME
from . import login_ui
from .unzip import unzip


class Download(QObject):
    """
        Generic http downloader using QNetworkAccessManager, supports login
    """

    replied = pyqtSignal(object)
    reply = pyqtSignal(object)
    done = pyqtSignal(str)
    cancel = pyqtSignal()
    error = pyqtSignal(str, str)
    progressChanged = pyqtSignal(float)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.request_is_aborted = False
        self.network = QNetworkAccessManager()
        self.network.authenticationRequired.connect(self._set_credentials)
        self.network.finished.connect(self._reply_finished)

        self.login = login_ui.LoginManager(SETTINGSNAME)
        self.file_requests = {}

    def get_file(self, url: str, filename: str, location: Path=None, credentials=(), cleanup=True, callback=None):
        """
        Will download a file from given URL, to target 'filename' path.
        If location path is given, it will attempt to unzip the file to the target path
        
        Args:
            url (str): URL to download file from
            filename (str): Path to download file to
            location (Path, optional): Will unzip to given path if given, Defaults to own folder
            credentials (tuple, optional): Tuple with download credentials in order:
                settings_tag, source name, registration link 
            cleanup (bool): Will cleanup zip if we unpacked it
        """

        self.request_is_aborted = False
        if credentials:
            self.login.set_credentials_info(*credentials)
        else:
            self.login.reset()

        message_log(f"Download from {url}")

        filename = str(filename)  # Pathlib
        request = QNetworkRequest(QUrl(url))

        # Object, to keep track of metadata for this get.
        data = SimpleNamespace()
        data.filename = filename
        data.url = url
        data.progress = 0
        data.location = location
        data.cleanup = cleanup
        data.callback = callback

        self.file_requests[url] = data
        request = self.network.get(request)
        self.hook_progress(request, data)
            
    def _set_credentials(self, reply: QNetworkReply, authenticator: QNetworkAccessManager):
        """Passes login data to QNetworkAccessManager
        
        Args:
            reply (QNetworkReply): network reply requesting login info
            authenticator (QNetworkAccessManager): network access manager to send info with
        """
        if not self.request_is_aborted:
            if self.login.username == None and self.login.password == None: 
                accept, username, password = self.login.show_ui()
                if accept:
                    authenticator = authenticator
                    authenticator.setUser(username)
                    authenticator.setPassword(password)
            else:
                authenticator = authenticator
                authenticator.setUser(self.login.username)
                authenticator.setPassword(self.login.password)
        else:
            self.request_is_aborted = True
            self.cancel.emit()
            reply.abort()

    def progress(self, bytes_read, bytes_total, data):
        if not bytes_total:
            bytes_total = 1  # Fix for some cases where bytes_total might give us 0

        # print(bytes_read, bytes_total, data)
        data.progress = bytes_read / bytes_total * 100
        total_progress = sum([request.progress for request in self.file_requests.values()]) / len(self.file_requests)
        self.progressChanged.emit(total_progress)

    def hook_progress(self, result, data):
        result.downloadProgress.connect(
            lambda b_read, b_total, reply=result: self.progress(b_read, b_total, data)
        )

    def _reply_finished(self, reply: QNetworkReply):
        """
            Reply is finished
        """
        if reply != None:
            redirect_url = reply.attribute(QNetworkRequest.RedirectionTargetAttribute)
            
            # If the URL is not empty, we're being redirected. 
            url = reply.request().url().toString()
            data = self.file_requests[url]
            
            if redirect_url != None:
                request = QNetworkRequest(redirect_url)

                # Replace namespace by updated URL
                data.url = redirect_url.toString()
                self.file_requests[redirect_url.toString()] = data

                del self.file_requests[url]
                result = self.network.get(request)
                self.hook_progress(result, data)
            else:
                if reply.error() != None:
                    if reply.error() ==  QNetworkReply.ContentNotFoundError:
                        reply.abort()
                        reply.deleteLater()
                        self.error.emit("DOWNLOAD FAILED" , "File not found on URL")
                        del self.file_requests[url]
                        
                    elif reply.error() ==  QNetworkReply.NoError:
                        result = reply.readAll()
                        try:
                            f = open(data.filename, 'wb')
                            f.write(result)
                            f.close()
                            del self.file_requests[url]
                            unpacked_file = unzip(data.filename, data.location, data.cleanup)
                            if unpacked_file is None:
                                self.error.emit("UNPACK ERROR", f"Failed to unpack {data.filename}")
                                return None
                                
                            if data.callback is not None:
                                result = data.callback(data.filename, unpacked_file)
                                if result is not None:
                                    unpacked_file = result

                            self.done.emit(unpacked_file)

                        except OSError:
                            drive = Path(data.filename).drive
                            self.error.emit("DOWNLOAD FAILED", f"OS Error, Drive '{drive}' full?")
                            return

                        
                        reply.deleteLater()
                    
