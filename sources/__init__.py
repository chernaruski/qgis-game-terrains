import json
from pathlib import Path
from typing import Tuple

from PyQt5.QtCore import pyqtSlot, QObject
from qgis import core
from qgis.core import QgsProject, QgsRasterLayer
from qgis.utils import iface
from qgis.gui import QgsMessageBarItem


from gameterraintools import gtt
from gameterraintools.extent import raise_extent, get_working_layer, transform_extent, NoMarkedAreaLayer
from gameterraintools.functions import get_layer_from_name, show_progress, set_layer_visibility
from gameterraintools.sources import heightmap, downloader, osm

def satmap_source(source: str):
    # Check for available sources
    source_file = Path(__file__).parent / "satmap.json"
    with source_file.open() as json_file:
        data = json.load(json_file)
    try:
        details = data[source]
    except KeyError:
        raise Exception(f"Unknown satmap source: {source}")
    return details

def zoom_source(source: str, zoom: int) -> str:
    """
        Gets zoomed version of satmap, 
        if source has zmin and zmax value replace them by our wanted zoom,
        and create a new layer with it to return, otherwise return the old layer
    """
    def replace(source, targets: tuple, value: int):
        parts = source.split('&')  # type: list
        for i, part in enumerate(parts):
            for target in targets:
                if part.startswith(target):
                    parts[i] = f"{target}={value}"
        return "&".join(parts)

    def find_max(source):
        parts = source.split('&')  # type: list
        for part in parts:
            if part.startswith('zmax'):
                try:
                    return int(part.split('=')[-1])
                except ValueError:
                    return -1
        return -1

    z_max = find_max(source)
    if z_max != -1:
        zoom_result = min([z_max, zoom])
        source = replace(source, ('zmin', 'zmax'), zoom_result)
        return (True, source)
    else:
        return (False, source)

def satmap_zoomed(layer: QgsRasterLayer, zoom: int, name='gtt_zoomed') ->  QgsRasterLayer:
    """
        Gets zoomed version of satmap, 
        if source has zmin and zmax value replace them by our wanted zoom,
        and create a new layer with it to return, otherwise return the old layer
    """
    source = layer.source()  # type: str
    zoomed, source = zoom_source(layer.source(), zoom)
    if zoomed:
        current = get_layer_from_name(name)
        if current is not None:
            QgsProject.instance().removeMapLayers([current.id()])

        layer = QgsRasterLayer(source, name, 'wms')
        QgsProject.instance().addMapLayer(layer)
        set_layer_visibility(layer, False)
        return layer
    else:
        return layer

class GttDataSource(QObject):

    progress: QgsMessageBarItem

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.progress = None

        # -------------------------------------------------------------------------------
        # Downloader
        self.downloader = downloader.Download(parent=self)
        self.downloader.progressChanged.connect(self._progress)
        self.downloader.done.connect(self.add_file)
        self.downloader.done.connect(self._done)
        self.downloader.cancel.connect(self._cancel)
        self.downloader.error.connect(self._error)

        # -------------------------------------------------------------------------------
        # Handling (OSM)
        self.osm_handling = osm.Handling(parent=self)
        # self.osm_handling.progressChanged.connect(self._progress)
        # self.osm_handling.done.connect(self.add_file)

        # -------------------------------------------------------------------------------
        # Downloader (OSM)
        self.osm_download = osm.downloader.Download(parent=self)
        # self.osm_download.progressChanged.connect(self._progress)
        self.osm_download.done.connect(self.osm_handling.run)

    def _progress(self, progress):
        if self.progress is not None:
            self.progress.bar.setValue(progress)
        if progress is None:  # Hide
            pass

    def _done(self, *args):
        if self.progress is not None:
            self.progress.setText("Downloaded heightmap")
            self.progress.bar.setValue(100)
            # self.progress.setDuration(5)

    def _cancel(self, *args):
        if self.progress is not None:
            self.progress.setText("Download cancelled")
            self.progress.bar.setRange(0, 0)
            self.progress.setDuration(5)

    def _error(self, title, msg):
        if self.progress is not None:
            iface.messageBar().popWidget(self.progress)
            self.progress = None
        iface.messageBar().pushCritical(title, msg)

    def add_file(self, filepath):
        iface.addRasterLayer(filepath, "gtt_heightmap")

    def add_satmap(self, source):
        """Adds given satmap to the canvas"""

        details = satmap_source(source)
        provider = details['provider']
        minzoom = details['minzoom']
        url = details['url'].replace('{minzoom}', str(minzoom))

        currentlayer = get_layer_from_name('gtt_satmap')
        if currentlayer is not None:
            QgsProject.instance().removeMapLayers([currentlayer.id()])

        layer = QgsRasterLayer(url, 'gtt_satmap', provider)
        QgsProject.instance().addMapLayer(layer)
        raise_extent()

    def add_heightmap(self, source):
        """Adds heightmap to the canvas"""
        if get_working_layer() is None:
            raise NoMarkedAreaLayer

        try:
            details = heightmap.sources[source]
        except KeyError:
            raise Exception(f"Unknown heightmap source: {source}")

        download_func = details['download']
        self.progress = show_progress("Downloading heightmap", range_=(0, 100))
        started = download_func(self.downloader)
        if not started:
            iface.messageBar().pushCritical("Missing data", f"No tiles available for source {source} on this extent")

        raise_extent()

    def add_osm(self, *args):
        """Downloads OSM roads and polgyons for marked extent"""

        if get_working_layer() is None:
            raise NoMarkedAreaLayer

        bounds = transform_extent("EPSG:4326")
        downloader = self.osm_download.download(bounds)
