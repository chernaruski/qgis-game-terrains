"""Save a geotff as rendered BMP with the style"""
from pathlib import Path
from typing import List

from ..process import background_process
from ..imports import system
from ..functions import handle_temp_name, get_temp_path, qgis_bin_folder

def save(path: Path, colors: List[tuple]) -> Path:
    """Saves given DEM-like tif file into a recolored BMP
    
    Args:
        path (Path): input file
        colors (List[tuple]): list of rgb colors
    """

    uid = system.get_uid()
    color_file = get_temp_path() / f"color_table_{uid}.txt"

    create_color_table(color_file, colors)
    output = path.parent / f'gtt_mask.{uid}.bmp'  # type: Path
    return_code = color_relief(path, color_file, output)
    if return_code == 0:
        output = handle_temp_name(output)
    return output


def color_relief(path: Path, color_file: Path, output: Path):
    """Runs gdaldem color-relief, to turn our singleband grey image into a properly colored Bitmap
    
    Args:
        path (Path): input file (classified mask)
        color_file (Path): Text file with color mapping
        output (Path): Target path for output file
    """

    gdal_path = qgis_bin_folder() / "gdaldem.exe"
    cmd = [
        str(gdal_path),
        "color-relief",
        str(path),
        str(color_file),
        str(output)
    ]
    return_code = background_process(cmd, log=print)

    # Cleanup useless XML file
    meta_file = output.with_suffix(".bmp.aux.xml")
    if meta_file.exists():
        try:
            meta_file.unlink()
        except Exception:
            pass

    return return_code


def create_color_table(color_file: Path, colors: List[tuple]):
    """Creates a file for gdaldem color relief option, each row consists of 4 elements like:
    band_value, red, green, blue
    
    The result is a txt file with contents like

        0 255 255 1
        1 6 182 0
        2 73 100 16
        3 0 28 171
        4 209 209 209
        
    Args:
        color_file (Path): Filepath to write to
        colors (List[tuple]): List of rgb colors
    """
    if not color_file.exists():
        color_file.touch(exist_ok=True)

    with color_file.open(mode='w') as file:
        i = 0
        for r, g, b in colors:
            file.write(f"{i} {r} {g} {b}\n")
            i += 1


