"""
    Handles the installation of the OrfeoToolbox
"""

import tempfile
import platform
from pathlib import Path
from functools import partial

from qgis.utils import iface
from qgis.core import Qgis
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QFileDialog

from gameterraintools.sources import downloader
from gameterraintools.functions import show_progress, get_temp_path, message_log
from gameterraintools import gtt

system = platform.system().lower()
if system == "windows":
    defaultdir = Path("C:\\")
    url = "https://www.orfeo-toolbox.org/packages/OTB-6.6.1-Win64.zip"
elif system == "linux":
    defaultdir = Path("opt")
    url = "https://www.orfeo-toolbox.org/packages/OTB-6.6.1-Linux64.run"
elif system == "darwin":
    defaultdir = Path("bin")
    url = "https://www.orfeo-toolbox.org/packages/OTB-6.6.1-Darwin64.run"

Url = Path(url)

def prepare(force=False):
    """
        Ask install location, downloads and unpacks. Async, so function isn't done yet when this function ends!
    """
    location = iface.gtt.settings['otb/location']
    if location in ("", "None", "OTB-6.6.1-Win64"):
        # Ask for install location if it's not set
        otb_path = get_install_folder()
        if otb_path is None:
            return None

        iface.gtt.setting_set('otb/location', str(otb_path))
    else:
        otb_path = Path(location)
        try:
            otb_path.mkdir(parents=True, exist_ok=True)
        except Exception as e:
            message_log(f"{e}", level=Qgis.Critical)
            message_log(f"Failed to install to path: {str(otb_path.resolve())}", level=Qgis.Critical)
            return False

    program = otb_path / 'bin' / 'otbApplicationLauncherCommandLine.exe'
    if program.exists() and not force:
        return True
    else:
        install(otb_path)

def check_installed():
    """Checks if otb is installed correctly"""
    location = iface.gtt.settings['otb/location']
    if location in ("", "None", "OTB-6.6.1-Win64"):
        return False
    else:
        program = Path(location) / 'bin' / 'otbApplicationLauncherCommandLine.exe'  # type: Path
        return program.exists()

def get_install_folder() -> Path:
    returndir = QFileDialog.getExistingDirectory(
        iface.mainWindow(), 
        "Select OrfeoToolbox install location", 
        str(defaultdir)
    )

    if returndir is "":
        return None

    # If we selected the OTB folder use that, if not, add the folder name right away
    otb_path = Path(returndir).resolve()
    if otb_path.name != Url.stem:
        otb_path = otb_path / Url.stem
    return otb_path


def install(path: Path, showprogress=True):
    """
        Downloads OrfeoToolbox to temp location and unpacks it, will show progress
    """
    if system in ("linux", "darwin"):
        raise NotImplementedError(f"{system} installation is currently not supported, please install it yourself and set the location in QGIS Settings")

    unzip_path = path.parent
    name = Path(url).name
    target_file = get_temp_path() / name
    target_file.parent.mkdir(parents=True, exist_ok=True)
    
    download = downloader.Download(parent=iface.gtt)
    download.get_file(url, target_file, unzip_path, cleanup=True)

    # Handle cleanup and progress reporting
    progressbar = None
    if showprogress:
        progressbar = show_progress("Downloading OrfeoToolbox", range_=(0, 100))
        def _progress(progressbar, progress):
            progressbar.bar.setValue(progress)
        download.progressChanged.connect(partial(_progress, progressbar))

    def _install_done(progressbar, download, path):
        if progressbar is not None:
            progressbar.setDuration(5)
            progressbar.setText("Succesfully downloaded OrfeoToolbox")
        download.deleteLater()

    def _error(progressbar, download, title, text):
        if progressbar is not None:
            progressbar.setDuration(5)
            progressbar.setText("Failed OrfeoToolbox download")

        iface.messageBar().pushCritical(title, text)
        message_log(f"{title} - {text}", level=Qgis.Critical)
        download.deleteLater()

    download.done.connect(partial(_install_done, progressbar, download))
    download.error.connect(partial(_error, progressbar, download))

