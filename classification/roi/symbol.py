"""
    Handles layer symbology (How a shapefile is represented on the map)
"""

from collections import OrderedDict

from qgis.core import QgsVectorLayer, QgsCategorizedSymbolRenderer, QgsRendererCategory, QgsFillSymbol
from qgis.core import QgsPalettedRasterRenderer
from qgis.utils import iface
from PyQt5.QtCore import QVariant
from PyQt5.QtGui import QColor


def add_symbol(layer: QgsVectorLayer, value: str, label: str, color: str):
    render = layer.renderer()  # type: QgsCategorizedSymbolRenderer
    category = create_symbol(value, color, label=label)  # type: QgsRendererCategory
    render.addCategory(category)
    # refresh
    iface.layerTreeView().refreshLayerSymbology(layer.id())
    return category


def remove_symbol(layer: QgsVectorLayer, label: str):
    """Removes the label with given label"""
    render = layer.renderer()  # type: QgsCategorizedSymbolRenderer
    for i, category in enumerate(render.categories()):
        if label == category.label():
            render.deleteCategory(i)
            return

    iface.layerTreeView().refreshLayerSymbology(layer.id())


def get_categories(layer: QgsVectorLayer):
    layer.renderer().categories()  # type: QgsCategorizedSymbolRenderer


def create_symbol(value: str, color: str, outline: str = 'black', label=None) -> QgsRendererCategory:
    """Creates a render symbol, controlling how the given value is drawn on the qgis canvas
    
    Args:
        value (str): Value to apply the symbol to
        color (str): Color to render the symbol with
        outline (str, optional): Outline color for the symbol. Defaults to 'black'.
        label (str, optional): Legend / UI label for this color. Defaults to value.
    
    Returns:
        QgsRendererCategory: The created symbology category
    """
    if label is None:
        label = str(value)
    symbol = QgsFillSymbol.createSimple({'color': color, 'outline_color': outline})
    category = QgsRendererCategory(value, symbol, label)
    return category


def update_style(layer: QgsVectorLayer, classes: OrderedDict):
    """Synchronizes the surface types to the layers symbology
    
    Args:
        layer (QgsVectorLayer): layer to set symbology to
        classes (dict): Dictionary with colors and names for surfaces
    """

    render = layer.renderer()  # type: QgsCategorizedSymbolRenderer
    render.deleteAllCategories()
       
    for clas in classes['classes'].values():
        color = clas['color']
        value = clas['name']
        category = create_symbol(value, color, label=value)  # type: QgsRendererCategory
        render.addCategory(category)

    category = create_symbol(None, "#000000", label='Other')
    render.addCategory(category)
    iface.layerTreeView().refreshLayerSymbology(layer.id())
    layer.triggerRepaint()
    iface.mapCanvas().refresh()


def create_banded_colors(layer: QgsVectorLayer, classes: OrderedDict):
    """
        Duplicates the categorized rendered of a shapefile to a paletted banded symbol,
        this will be used on the grayscale classified .tif to render as arma mask
    """

    symbols = []
    index = 0
    for clas in classes['classes'].values():
        symbols.append(QgsPalettedRasterRenderer.Class(index, QColor(clas['color']), clas['name']))
        index += 1
    render = QgsPalettedRasterRenderer(layer.dataProvider(), 1, symbols)
    layer.setRenderer(render)
    layer.triggerRepaint()
    iface.layerTreeView().refreshLayerSymbology(layer.id())