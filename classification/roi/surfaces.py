import json
from collections import OrderedDict
from typing import List
from pathlib import Path
from shutil import copy

from PyQt5.QtGui import QColor

from gameterraintools.functions import settings_path

def load_surfaces() -> OrderedDict:
    """Handles json file we save our layer defintions in"""
    path = settings_path()  # type: Path
    path.mkdir(parents=True, exist_ok=True)

    file = path / 'roi_classes.json'
    if not file.exists():
        copy_settings_file(file)

    with file.open('r') as fp:
        data = fp.read()

    if data == "":
        copy_settings_file(file)

    with file.open('r') as fp:
        data = json.load(fp, object_pairs_hook=OrderedDict)
    return data


def copy_settings_file(target: Path):
    copy(str(Path(__file__).parent / 'shp' / 'roi_classes.json'), str(target))


def save_settings_file(classes: OrderedDict):
    """Handles json file we save our layer defintions in"""
    path = settings_path()  # type: Path
    path.mkdir(parents=True, exist_ok=True)

    file = path / 'roi_classes.json'
    with file.open('w') as fp:
        json.dump(classes, fp, indent=2)


def readable_list() -> List[str]:
    surfaces = load_surfaces()
    lines = []
    for surface in surfaces['classes'].values():
        color = surface['color']
        name = surface['name']
        r, g, b, a = QColor(color).getRgb()

        line = f"{name} = ({r}, {g}, {b})  {color}"
        lines.append(line)
    return lines