from pathlib import Path
from collections import OrderedDict
import shutil

from qgis.core import QgsVectorLayer, QgsProject, QgsFeature
from qgis.core import QgsCoordinateReferenceSystem, QgsVectorFileWriter
from qgis.utils import iface
from PyQt5.QtGui import QColor
from PyQt5.QtCore import QVariant

from gameterraintools import gtt
from gameterraintools.functions import project_folder, get_layer_from_name
from .surfaces import load_surfaces
from .symbol import update_style

def copy_layer(target: Path) -> Path:
    """Copies the empty gtt_roi sample layer to given path
    
    Args:
        target (Path): folder path where shapefile shouold be copied to
    
    Returns:
        Path: path to .shp file in new location
    """

    source = Path(__file__).parent / 'shp' / 'gtt_roi'  # type: Path
    for suffix in ("dbf", "prj", "qpj", "shx", "shp"):
        source_file = source.with_suffix("." + suffix)
        target_file = target / source_file.name
        shutil.copy(source_file, target_file)
    return target_file


def create_layer() -> QgsVectorLayer:
    """
        Creates the ROI layer, used to hold shapes and layer symobology
        Will return existing layer if one already exists
    
    Returns:
        QgsVectorLayer: gtt_roi layer
    """
    project = project_folder()
    if str(project) == ".":
        iface.messageBar().pushCritical("Error", "Please save the QGIS Project before marking area")
        return None

    layer = get_layer_from_name('gtt_roi')
    if layer is None:
        path = copy_layer(project)
        layer = QgsVectorLayer(str(path), 'gtt_roi', 'ogr')
        layer.loadNamedStyle(str(Path(__file__).parent / 'shp' / 'style_base.qml'))
        QgsProject.instance().addMapLayer(layer)

        # Updates the style
        surfaces = load_surfaces()
        update_style(layer, surfaces)

    return layer


def random_color() -> QColor:
    """Creates a random RGB color
    
    Returns:
        QColor: random rgba color
    """
    from random import uniform
    colors = [int(uniform(0, 255)) for i in range(0, 3)]
    return QColor(*colors)


def sync_attributes(classes: OrderedDict, layer: QgsVectorLayer) -> bool:
    """Writes integer index of surface type for each feature in the given layer
    Surface names are easier to use, but OTB requires a integer identifier, so we'll sync it here
    
    Args:
        classes (OrderedDict): Ordered dict with names and colors of surfaces
        layer (QgsVectorLayer): gtt_roi layer
    """
    surfaces = []
    missing = []
    for clas in classes['classes'].values():
        surfaces.append(clas['name'].lower())

    layer.startEditing()
    type_index = layer.fields().lookupField('type')  # type: int
    for feat in layer.getFeatures():
        if feat.geometry().isNull():
            layer.deleteFeatures([feat.id()])
            continue
        if feat['class']:
            try:
                surface_index = surfaces.index(feat['class'].lower())
                layer.changeAttributeValue(feat.id(), type_index, surface_index)
            except ValueError:
                missing.append(feat['class'])
    layer.commitChanges()
    return True
