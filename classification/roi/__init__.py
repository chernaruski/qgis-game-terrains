"""
    Functions to create Regions of Interest shapefile layer,
    prevent users from requiring to know about shapefiles attributes,
    plus support for arma config stuff
"""

import json
from pathlib import Path
from collections import OrderedDict

from PyQt5.QtCore import Qt
from qgis.utils import iface

from .widget import RoiWindow

def create_wizard():
    iface.gtt_plugin.widgetCreate('roi', RoiWindow, tools=iface.gtt)
