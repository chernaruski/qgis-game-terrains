"""
    Subprocess calls and output handling, plus environment setup 
"""

# import subprocess
import os

from pathlib import Path
from qgis.utils import iface
from ..process import background_process

def setup_env(folder: Path):
    """Sets up the environment variables required to run OTB"""
    env = os.environ.copy()  # type: dict
    memlimit = iface.gtt.settings['otb/memory']
    values = {
        'env': {
            'LC_NUMERIC': 'C', 
            'GDAL_DRIVER_PATH': 'disable', 
            'GDAL_DATA': str(folder / 'share' / 'data'), 
            'GEOTIFF_CSV': str(folder / 'share' / 'epsg_csv'), 
            'OTB_LOGGER_LEVEL': 'INFO', 
            'OTB_MAX_RAM_HINT': str(memlimit)
        }, 
        'encoding': 'cp1252'
    }

    for key, value in values['env'].items():
        env[key] = value
    return env

def parse(args):
    """Parses the arguments"""

    if isinstance(args, dict):
        args_handled = []
        for key, value in args.items():
            args_handled.extend((f"-{key}", f'{value}'))
        args = args_handled
    return args

def run(folder: Path, alg: str, args: list, log=print, heartbeat=False, env=None) -> int:
    """Runs given alg with args in background thread,ing, communicating back the progress through log callable
    
    Args:
        folder (Path): OTB folder
        alg (str): Algorhitm to
        args (list): Arguments to give
        log (callable, optional): Callable function to handle the STDOUT with. Defaults to print.
        heartbeat (bool, optional): Will create output to indicate process is still running. Defaults to False.

    Returns:
        int: Return code, 0 for success. 
    """

    env = setup_env(folder)
    exe = str(folder / 'bin' / 'otbApplicationLauncherCommandLine.exe')
    apps = str(folder / 'lib' / 'otb' / 'applications')
    cmd = [exe, alg, apps] + parse(args)
    return_code = background_process(cmd, env, log, heartbeat)
    return return_code

