"""
    Loads in an Arma TB-style txt file and makes shapes for it
"""

from pathlib import Path

from PyQt5.QtCore import QVariant
from PyQt5.QtWidgets import QFileDialog
from qgis.utils import iface
from qgis.core import QgsVectorLayer, QgsFeature, QgsFields, QgsField
from qgis.core import QgsCoordinateReferenceSystem, QgsVectorFileWriter, QgsWkbTypes
from qgis.core import QgsGeometry, QgsPointXY, QgsSettings


from ..functions import project_folder
from ..static import SETTINGSNAME

def import_file(library):
    path = select_file()
    if path:
        result = create_layer(Path(path), library)
        created = result['created']
        iface.messageBar().pushSuccess("OBJECTS", f"Import succesful, created {created} objects")


def select_file() -> str:
    """Selects BMP file, and turns it into an amazing mask"""
    options = QFileDialog.Options()

    settings = QgsSettings()
    dir_last = settings.value(f"{SETTINGSNAME}/objects/import_path", None, type=str)
    if dir_last == "" or not Path(dir_last).is_dir():
        path = project_folder()
    else: 
        path = Path(dir_last)


    data = QFileDialog().getOpenFileName(
        parent=iface.mainWindow(), 
        caption="Select object file", 
        directory=str(path),
        filter="TB Object file (*{})".format(".txt"),
        options=options
    )
    file = data[0]
    if file:
        path = Path(file).parent
        settings.setValue(f"{SETTINGSNAME}/objects/import_path", str(path))
    return file


def create_object_writer(path: Path, crs_id: str, fields: QgsFields):
    target_crs = QgsCoordinateReferenceSystem(crs_id)
    writer = QgsVectorFileWriter(str(path), "utf-8", fields, QgsWkbTypes.Point, target_crs, "ESRI Shapefile")
    return writer


def create_layer(object_path: Path, library) -> dict:
    path = object_path.parent / f'{object_path.stem}.shp'
    utm = 'EPSG:32631'
    fields = get_fields()
    writer = create_object_writer(path, utm, fields)
    objects = []

    i = 0

    for data in load_file(object_path):
        x = data['position'][0]
        y = data['position'][1]
        z = data['position'][2]

        category = library.get_model_category(data['model'])

        feature = QgsFeature(fields, i)
        feature.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(x, y)))
        feature.setAttributes([i, x, y, z, data['model'], data['scale'], category])
        writer.addFeature(feature)
        i += 1

    iface.addVectorLayer(str(path), 'gtt_', "ogr")
    del writer

    return { "created": i }



def get_fields():
    fields = QgsFields()
    fields.append(QgsField('ID', QVariant.Int))
    fields.append(QgsField('X', QVariant.Double))
    fields.append(QgsField('Y', QVariant.Double))
    fields.append(QgsField('Z', QVariant.Double))
    fields.append(QgsField('MODEL', QVariant.String))
    fields.append(QgsField('SCALE', QVariant.Double))
    fields.append(QgsField('library', QVariant.String))
    return fields


def from_tb_format(line: str) -> dict:
    """Constructs a tb object from a import/export line"""
    data = line.strip('\n').split(";")
    model = data[0]

    datafloat = [float(f) for f in data[1:8]]
    datafloat.insert(0, data[0])
    position = [datafloat[1], datafloat[2], datafloat[7]]
    rotation = [datafloat[4], datafloat[5], datafloat[3]]
    items = {
        "position": position, 
        "rotation": rotation, 
        "model": data[0].replace('"', ''),
        "scale": datafloat[6]
    }
    return items


def load_file(path: Path):
    """Loads in an object file"""
    with path.open(mode='r') as fp:
        for line in fp:
            yield from_tb_format(line)

