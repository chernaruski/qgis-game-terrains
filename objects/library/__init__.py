"""Arma / Terrain builder format handling"""

from pathlib import Path
import logging
from typing import List, Optional, Tuple
from .colors import tbcolor_to_arma

try:
    import xmltodict
    imported = True
except ModuleNotFoundError:
    imported = False

logger = logging.getLogger(__name__)

def load_library_file(path: Path) -> Tuple[int, dict]:
    """Imports a TB library file in xml format and creates dictionary out of it"""
    if not path.exists():
        return (1, {})

    try:
        with path.open(mode='r') as fp: 
            library = xmltodict.parse(fp.read())
            # xmldict doesn't given reliable result if there's only one entry, so do this to fix it
        logger.info("loaded library: {}".format(path.name))
        return (0, library)
    except:
        return (1, {})

def get_library_entries(library):
    """Gets template entries in a library file (One entry per model path)"""
    try:
        _data = [library['Library']['@name'], ['_templateName', '_modelPath', '_slopeLandContact']]
        library['Library']['Template']
    except (KeyError, TypeError):
        return []

    # Unfuck xmltodict behaviour when there's only one element in the template
    try:
        libraryentries = library['Library']['Template']
        libraryentries[0]['Name']
    except:
        libraryentries = [library['Library']['Template']]
    return libraryentries

def parse_library(library):
    """Parses the xml library to list format we want for A3"""
    # Test if library has template entry, empty .tml files won't have this
    try:
        name = library['Library']['@name']
        rectangle = library['Library']['@shape'] == 'rectangle'
        data = [[name, rectangle], ['_templateName', '_modelPath', '_slopeLandContact', '_color']]
        library['Library']['Template']
    except (KeyError, TypeError):
        return []

    # Unfuck xmltodict behaviour when there's only one element in the template
    libraryentries = get_library_entries(library)
    logger.info("Parsing library {}, entry count: {}".format(name, len(libraryentries)))

    for template in libraryentries:
        try:
            _name = template['Name']
            modelpath = template['File']
            placementtype = template['Placement']
            color = tuple(round(i, 3) for i in tbcolor_to_arma(int(template['Fill'])))

        except Exception as e:
            print(e)
            _name = 'Broken Template'
            modelpath = ''
            color = (1, 1, 1, 1)

        if placementtype is None:
            placementtype = ""
        else:
            placementtype = str(placementtype)
        slopelandcontact = int(placementtype.lower() == "slopelandcontact")
        data.append([template['Name'], modelpath, slopelandcontact, color])
    return data



class tbLibraryCache(object):
    """Loads in an entire folder of .tml files into a dictionary"""

    def __init__(self, path: Path, verbose=True):
        """Do stuff"""
        self.modeldict = {}
        self.categories = []
        self.verbose = verbose
        self.parse_library_files(path)

    def __getitem__(self, key):
        return self.modeldict[key]

    def __setitem__(self, key, value):
        self.modeldict[key] = value

    def get_library_files(self, path: Path, extension="tml") -> List[Path]:
        files = []
        for filepath in path.glob(f"*.{extension}"):
            if self.verbose:
                print(filepath.name)
            files.append(filepath)
        return files

    def get_model_category(self, modelname: str) -> str:
        """Gets the library category this model is in, uses cache"""
        try:
            category = self.modeldict[modelname.lower()]
        except KeyError:
            if self.verbose:
                print("{} not found in any template file".format(modelname))
            category = ""
        return category

    def get_model_categories(self):
        """Gets all the categories"""
        return []

    def cache_template(self, library):
        libraryentries = get_library_entries(library)

        if len(libraryentries) == 0:
            return 0

        category = library['Library']['@name']
        if category not in self.categories:
            self.categories.append(category)
        for template in libraryentries:
            try:
                templatename = template['Name']
                self.modeldict[templatename.lower()] = library['Library']['@name']

            except Exception as _e:
                pass
        return 0

    def parse_library_files(self, path):
        """Set the entries from the file into a dictionary"""
        if self.verbose:
            print("\n## Listing files in Folder ##")

        if not path.is_dir():
            if self.verbose:
                print("Invalid folder: {}".format(path))
            return 1

        libraryfiles = self.get_library_files(path)
        try:
            for filepath in libraryfiles:
                retval, librarydict = load_library_file(filepath)
                if retval > 0:
                    raise Exception(retval)
                retval = self.cache_template(librarydict)

            return 0
        except:
            return 1