from pathlib import Path

from PyQt5.QtWidgets import QFileDialog
from qgis.utils import iface

def ask_library_path(path: Path) -> str:
    location = QFileDialog.getExistingDirectory(
        parent=iface.mainWindow(),
        caption='Select TML file directory',
        directory=str(path)
    )
    return location

