
from qgis.utils import iface
from .install import install_requirements
if __name__ == "gameterraintools":
    install_requirements()

from .plugin import GameTerrainTools

# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load GameTerrainTools class from file GameTerrainTools.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    return GameTerrainTools(iface)
