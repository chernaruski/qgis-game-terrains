from .arma import EngineArma
from .ue4 import  EngineUE4


engines = (
    EngineArma(),
    EngineUE4(),
)