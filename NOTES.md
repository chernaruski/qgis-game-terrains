## Notes

### Heightmap exporting 
    * Cut the (multiple) heightmaps to extent area + 10% size, set -nodata to -9999, set to bicubic, change CRS to proper UTM
    * Merge the resulting heightmaps 
    * Scale the merged heightmap to proper cell size (with bicubic)
    * CUt the scaled heightmap to exact dimensions