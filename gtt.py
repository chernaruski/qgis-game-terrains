import os
import math
from pathlib import Path
import threading

from PyQt5.QtCore import QObject, QSettings, Qt, pyqtSignal
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QSpinBox

from qgis import core
from qgis.utils import iface
from qgis.core import QgsSettings

from .static import SETTINGSNAME, PROJECT_URL
from .gtt_markarea import MarkAreaTool
from .functions import get_layer_from_name, create_transform_layer, move_layer_to_top, message_log, plugin_path
from .functions import get_temp_path
from .functionsmisc import reset_settings, clear_cache

from .ui.functions import add_menu_action
from .sources import GttDataSource
from .exporter import GttExport
from .engine.base import Engine
from .install import update_available



class GttTools(QObject):
    setting_changed = pyqtSignal(str, object)
    version_check_done = pyqtSignal(bool)
    engine: Engine

    def __init__(self, parent=iface):
        super().__init__(parent=parent)

        self.actions = []
        self.loadSettings()
        self.sources = GttDataSource(parent=self)
        self.engine = Engine()
        iface.gtt = self

        self.context = core.QgsProcessingContext()
        self.context.setProject(core.QgsProject.instance())
        self.feedback = core.QgsProcessingFeedback()
        self.addModules()
        self.loadActions()

        self.version_check_done.connect(self._versionNotify)
        thread = threading.Thread(target=self.versionCheck)
        thread.start()

    def versionCheck(self, manual=False):
        if update_available():
            self.version_check_done.emit(True)
        else:
            if manual:
                self.version_check_done.emit(False)
        
    def _versionNotify(self, update):
        if update:
            iface.messageBar().pushMessage(f"A Game Terrain Tools update is available on <a href='{PROJECT_URL}'>Gitlab</a>", duration=20)
            message_log("A Game Terrain Tools update is available on gitlab")
        else:
            iface.messageBar().pushSuccess("GTT", "Latest version of GTT is installed")
            message_log("Latest version of Game Terrain Tools already installed")


    def addModules(self):
        self.exporter = GttExport(self, parent=self)
        self.markarea = MarkAreaTool(iface.mapCanvas(), self)
        self.sources = GttDataSource(parent=self)

    def loadActions(self):
        self.actions.append(add_menu_action(reset_settings, "Reset settings"))
        self.actions.append(add_menu_action(clear_cache, "Clear download cache"))
        self.actions.append(add_menu_action(lambda: self.versionCheck(True), "Check for updates"))

    def unload(self):
        for action in self.actions:
            iface.removePluginMenu('Game Terrain Tools', action)

    def setProgress(self, value: int):
        widget = iface.gtt_plugin.widget('primary')
        if widget is not None:
            widget.export_progressbar.setValue(value)
            

    #--------------------------------------------------------------------------
    def loadSettings(self):
        """
            Loads all the QSettings used for this plugin.
            Register non-set settings, so we can configure them in QGIS preferences
        """

        self.settings = {}
        settings_registered = {
            "engine": "Arma",
            "widget_open": True,
            "primary_tabs": 0,

            "mapsize": 4096,
            "tilesize": 4096,
            "tile_cleanup": True,
            'satmap': 'bing',
            "zoom_level": 17,
            "scaled": False,
            "scaled_size": 4096,
            "map_reso": -1,
            "tilesize_enabled": False,
            "export_satmap": True,

            "export_mask": True,
            "mask_type": 'osm',
            "otb/memory": 2048,
            "otb/location": "",

            "export_roads": True,
            "export_height": True,
            "scaled_height": True,
            "height_reso": 4096,
            "heightmap": 'srtm',
            "advanced": 0
        }

        self._settings = settings = QgsSettings()

        print(f"Restoring settings '{SETTINGSNAME}'")
        for key, default in settings_registered.items():

            # We do dont give a type initially, so we can check if a setting exists, 
            # instead of retrieving a value of the default type (0 for int for example, 
            # which might be the actual existing setting)
            setting = settings.value(f"{SETTINGSNAME}/{key}", None)
            if setting is not None:
                setting = settings.value(f"{SETTINGSNAME}/{key}", None, type=type(default))
            else:
                settings.setValue(f"{SETTINGSNAME}/{key}", default)
                setting = default

            print(f"'{key}' = {setting}  [{default}]")
            self.settings[key] = setting


    def setting_set(self, setting, value):
        self.settings[setting] = value
        self._settings.setValue(f"{SETTINGSNAME}/{setting}", value)
        self.setting_changed.emit(setting, value)
        return value


    def mark_area(self):
        self.markarea.draw_square()

    def export(self, types):
        message_log("<b>STARTING EXPORT</b>")
        self.exporter.export(types=types)


