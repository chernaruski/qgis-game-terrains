"""Misc functions to do various things, mainly technical stuff called from Plugins menu"""

from qgis.core import QgsSettings
from qgis.utils import iface

# from .gtt import GttTools
from .static import SETTINGSNAME
from .functions import get_temp_path, message_log

def get_tools():
    if hasattr(iface, 'gtt'):
        return iface.gtt
    else:
        None

def reset_settings(*args, **kwargs):
    """Removes all QgsSettings related to the tool, then reset them to defaults"""

    settings = QgsSettings()
    settings.remove(SETTINGSNAME)
    tools = get_tools()
    if tools is not None:
        tools.loadSettings()

    iface.messageBar().pushSuccess('GTT', "Reset to default settings")


def clear_cache(*args, **kwargs):
    """ Clears the plugin data folder"""
    deleted = 0
    temp = get_temp_path()
    for file in temp.rglob('*'):
        try:
            (temp / file).unlink()
            deleted += 1
        except Exception as e:
            pass

    if deleted:
        iface.messageBar().pushSuccess('GTT', f"Deleted {deleted} files")
        message_log(f"Deleted {deleted} files")
