<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" maxScale="0" minScale="0" labelsEnabled="0" readOnly="0" simplifyAlgorithm="0" simplifyDrawingTol="1" simplifyDrawingHints="1" hasScaleBasedVisibilityFlag="0" simplifyMaxScale="1" version="3.8.0-Zanzibar" simplifyLocal="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="RuleRenderer" symbollevels="1" enableorderby="0" forceraster="0">
    <rules key="{c1e5db62-f8b8-467d-a9c9-f2a1c557050e}">
      <rule key="{5a21fc8a-276a-49d4-9118-f0c235c32831}" label="Farm Grass" filter="&quot;landuse&quot; in ('farmland', 'animal_keeping', 'orchard', 'Farm_land') OR &quot;place&quot; in ('farm')" symbol="0"/>
      <rule key="{2f3c2c11-8a64-47a6-81b5-023da0199d93}" label="Farm Grass" filter="&quot;landuse&quot; in ('plant_nursery')" symbol="1"/>
      <rule key="{318755ac-a828-4a17-b8b9-74cf8868d7a9}" label="Residential Grass" filter="&quot;landuse&quot; in ('farmyard', 'residential', 'allotments', 'vineyard', 'greenfield', 'greenhouse_horticulture', 'retail') OR &quot;leisure&quot; in ('playground')" symbol="2"/>
      <rule key="{38c9445d-f8c5-4da2-bc2c-57b72892bde3}" label="Clean Grass" filter="&quot;leisure&quot; in ('pitch', 'stadium', 'recreation_ground') OR &quot;landuse&quot; in ('cemetery') OR &quot;natural&quot; in ('grassland')" symbol="3"/>
      <rule key="{deb8de9a-b0ce-47ca-a5a8-54d5e1ac90ae}" label="Grass" filter="&quot;landuse&quot; in ('grass')" symbol="4"/>
      <rule key="{653f0a61-b91a-468d-94be-333f3a34d7c1}" label="Trees" filter="&quot;landuse&quot; in ('forest', 'meadow') OR &quot;natural&quot; in ('wood')" symbol="5"/>
      <rule key="{602a97ed-ca9b-4aed-a9ec-b7a212c23779}" label="Heath" filter="&quot;natural&quot; in ('heath')" symbol="6"/>
      <rule key="{58f6a15a-352c-49ce-8c0b-960837827858}" label="Parks" filter="&quot;natural&quot; in ('scrub', 'tree_row') OR &quot;leisure&quot; in ('garden', 'park', 'common') OR &quot;landuse&quot; in ('village_green')" symbol="7" checkstate="0"/>
      <rule key="{8c9077ec-9286-49f5-9979-0f7af61a9d61}" label="Industrial" filter="&quot;landuse&quot; in ('industrial', 'depot', 'port') OR &quot;amenity&quot; in ('parking') OR &quot;aeroway&quot; in ('aerodrome', 'helipad')" symbol="8"/>
      <rule key="{1b3ec8b6-d73a-4df4-97b1-503a7a2d33b2}" label="Buildings" filter="&quot;building&quot; != ''" symbol="9" checkstate="0">
        <rule key="{20a47b57-3a13-4998-a364-13e79ac7e349}" label="&quot;building&quot; = 'yes'" filter="&quot;building&quot; = 'yes'" symbol="10" checkstate="0"/>
        <rule key="{971bee0b-73d8-4055-ad24-cb099a178574}" label="&quot;building&quot; = ''" filter="&quot;building&quot; = ''" symbol="11" checkstate="0"/>
      </rule>
      <rule key="{cb3843dd-37f9-4cc5-b051-9a3408bbffd8}" label="Dirt" filter="&quot;landuse&quot; in ('construction', 'landfill', 'quarry', 'brownfield')" symbol="12"/>
      <rule key="{a99c7641-9fc6-4813-973b-01f421bc44ea}" label="Flats" filter="&quot;landuse&quot; in ('salt_pond')" symbol="13"/>
      <rule key="{d063bd82-36d0-4ecd-946c-62f339cd1413}" label="Non-special classed" filter="&quot;landuse&quot; in ('military')" symbol="14"/>
      <rule key="{12443d0b-4c5a-4901-9660-08c5aac61e9d}" label="Boundaries" filter="&quot;boundary&quot; != '' " symbol="15"/>
      <rule key="{48fee59a-0d9a-446d-a5f0-433ea9b2d9a7}" label="Neighbourhood" filter="&quot;place&quot; in ('neighbourhood')" symbol="16"/>
      <rule key="{abc013d7-f034-4268-97dc-10bb0d50a690}" label="Water" filter="&quot;natural&quot; in ('water', 'wetland') OR &quot;leisure&quot; in ('marina') OR &quot;landuse&quot; in ('reservoir', 'basin', 'aquaculture', 'pond', 'fishfarm')" symbol="17"/>
      <rule key="{fdf553dc-74f0-43ee-baa9-dbf12b6e1ac9}" label="Beach" filter="&quot;natural&quot; in ('beach', 'harbour')" symbol="18"/>
      <rule key="{1de756a7-5154-489c-ab81-6144c548cde7}" label="Rocks" filter="&quot;natural&quot; in ('coastline', 'bare_rock')" symbol="19"/>
      <rule key="{4d825750-3ad4-405c-aafd-015d24e6c6be}" label="River" filter="&quot;other_tags&quot; = '&quot;waterway&quot;=>&quot;riverbank&quot;'" symbol="20"/>
      <rule key="{0cf73a1a-96b1-486e-a149-0a0f6e0db1d3}" label="Other" filter="ELSE" symbol="21" checkstate="0"/>
    </rules>
    <symbols>
      <symbol force_rhr="0" name="0" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="15" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="128,197,46,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="1" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="15" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="128,197,46,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="10" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="58,52,236,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="11" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="238,110,71,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="12" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="30" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="126,62,6,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="13" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="30" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="126,62,6,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="14" type="fill" alpha="0" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="154,165,238,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="15" type="fill" alpha="0" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="154,165,238,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="16" type="fill" alpha="0" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="154,165,238,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="17" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="40" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="91,137,223,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="18" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="40" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="91,137,223,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="19" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="40" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="91,137,223,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="2" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="10" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="80,148,55,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="20" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="40" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="91,137,223,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="21" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="11" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="233,58,236,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="3" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="10" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="80,148,55,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="4" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="10" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="80,148,55,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="5" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="15" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="0,137,23,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="6" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="15" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="0,137,23,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="7" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="40" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="0,137,23,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="8" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="5" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="249,44,212,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="no"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol force_rhr="0" name="9" type="fill" alpha="1" clip_to_extent="1">
        <layer locked="0" enabled="1" pass="0" class="SimpleFill">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="207,63,99,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.06"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" name="name" type="QString"/>
              <Option name="properties"/>
              <Option value="collection" name="type" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory lineSizeScale="3x:0,0,0,0,0,0" lineSizeType="MM" penColor="#000000" backgroundColor="#ffffff" penWidth="0" backgroundAlpha="255" height="15" minScaleDenominator="0" diagramOrientation="Up" scaleDependency="Area" width="15" sizeType="MM" rotationOffset="270" maxScaleDenominator="0" penAlpha="255" opacity="1" sizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" barWidth="5" minimumSize="0" scaleBasedVisibility="0" enabled="0">
      <fontProperties description="MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0" style=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" zIndex="0" placement="1" linePlacementFlags="18" priority="0" dist="0" obstacle="0">
    <properties>
      <Option type="Map">
        <Option value="" name="name" type="QString"/>
        <Option name="properties"/>
        <Option value="collection" name="type" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="osm_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_way_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="aeroway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="amenity">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="admin_leve">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="barrier">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boundary">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="craft">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="geological">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="historic">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="land_area">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="landuse">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="leisure">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="man_made">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="military">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="natural">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="office">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="place">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="shop">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sport">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tourism">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="other_tags">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="osm_id"/>
    <alias index="1" name="" field="osm_way_id"/>
    <alias index="2" name="" field="name"/>
    <alias index="3" name="" field="type"/>
    <alias index="4" name="" field="aeroway"/>
    <alias index="5" name="" field="amenity"/>
    <alias index="6" name="" field="admin_leve"/>
    <alias index="7" name="" field="barrier"/>
    <alias index="8" name="" field="boundary"/>
    <alias index="9" name="" field="building"/>
    <alias index="10" name="" field="craft"/>
    <alias index="11" name="" field="geological"/>
    <alias index="12" name="" field="historic"/>
    <alias index="13" name="" field="land_area"/>
    <alias index="14" name="" field="landuse"/>
    <alias index="15" name="" field="leisure"/>
    <alias index="16" name="" field="man_made"/>
    <alias index="17" name="" field="military"/>
    <alias index="18" name="" field="natural"/>
    <alias index="19" name="" field="office"/>
    <alias index="20" name="" field="place"/>
    <alias index="21" name="" field="shop"/>
    <alias index="22" name="" field="sport"/>
    <alias index="23" name="" field="tourism"/>
    <alias index="24" name="" field="other_tags"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default applyOnUpdate="0" expression="" field="osm_id"/>
    <default applyOnUpdate="0" expression="" field="osm_way_id"/>
    <default applyOnUpdate="0" expression="" field="name"/>
    <default applyOnUpdate="0" expression="" field="type"/>
    <default applyOnUpdate="0" expression="" field="aeroway"/>
    <default applyOnUpdate="0" expression="" field="amenity"/>
    <default applyOnUpdate="0" expression="" field="admin_leve"/>
    <default applyOnUpdate="0" expression="" field="barrier"/>
    <default applyOnUpdate="0" expression="" field="boundary"/>
    <default applyOnUpdate="0" expression="" field="building"/>
    <default applyOnUpdate="0" expression="" field="craft"/>
    <default applyOnUpdate="0" expression="" field="geological"/>
    <default applyOnUpdate="0" expression="" field="historic"/>
    <default applyOnUpdate="0" expression="" field="land_area"/>
    <default applyOnUpdate="0" expression="" field="landuse"/>
    <default applyOnUpdate="0" expression="" field="leisure"/>
    <default applyOnUpdate="0" expression="" field="man_made"/>
    <default applyOnUpdate="0" expression="" field="military"/>
    <default applyOnUpdate="0" expression="" field="natural"/>
    <default applyOnUpdate="0" expression="" field="office"/>
    <default applyOnUpdate="0" expression="" field="place"/>
    <default applyOnUpdate="0" expression="" field="shop"/>
    <default applyOnUpdate="0" expression="" field="sport"/>
    <default applyOnUpdate="0" expression="" field="tourism"/>
    <default applyOnUpdate="0" expression="" field="other_tags"/>
  </defaults>
  <constraints>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="osm_id"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="osm_way_id"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="name"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="type"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="aeroway"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="amenity"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="admin_leve"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="barrier"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="boundary"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="building"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="craft"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="geological"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="historic"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="land_area"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="landuse"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="leisure"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="man_made"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="military"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="natural"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="office"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="place"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="shop"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="sport"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="tourism"/>
    <constraint notnull_strength="0" constraints="0" exp_strength="0" unique_strength="0" field="other_tags"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="osm_id"/>
    <constraint desc="" exp="" field="osm_way_id"/>
    <constraint desc="" exp="" field="name"/>
    <constraint desc="" exp="" field="type"/>
    <constraint desc="" exp="" field="aeroway"/>
    <constraint desc="" exp="" field="amenity"/>
    <constraint desc="" exp="" field="admin_leve"/>
    <constraint desc="" exp="" field="barrier"/>
    <constraint desc="" exp="" field="boundary"/>
    <constraint desc="" exp="" field="building"/>
    <constraint desc="" exp="" field="craft"/>
    <constraint desc="" exp="" field="geological"/>
    <constraint desc="" exp="" field="historic"/>
    <constraint desc="" exp="" field="land_area"/>
    <constraint desc="" exp="" field="landuse"/>
    <constraint desc="" exp="" field="leisure"/>
    <constraint desc="" exp="" field="man_made"/>
    <constraint desc="" exp="" field="military"/>
    <constraint desc="" exp="" field="natural"/>
    <constraint desc="" exp="" field="office"/>
    <constraint desc="" exp="" field="place"/>
    <constraint desc="" exp="" field="shop"/>
    <constraint desc="" exp="" field="sport"/>
    <constraint desc="" exp="" field="tourism"/>
    <constraint desc="" exp="" field="other_tags"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortOrder="0" sortExpression="">
    <columns>
      <column hidden="0" name="osm_id" width="-1" type="field"/>
      <column hidden="0" name="osm_way_id" width="-1" type="field"/>
      <column hidden="0" name="name" width="-1" type="field"/>
      <column hidden="0" name="type" width="-1" type="field"/>
      <column hidden="0" name="aeroway" width="-1" type="field"/>
      <column hidden="0" name="amenity" width="-1" type="field"/>
      <column hidden="0" name="admin_leve" width="-1" type="field"/>
      <column hidden="0" name="barrier" width="-1" type="field"/>
      <column hidden="0" name="boundary" width="-1" type="field"/>
      <column hidden="0" name="building" width="-1" type="field"/>
      <column hidden="0" name="craft" width="-1" type="field"/>
      <column hidden="0" name="geological" width="-1" type="field"/>
      <column hidden="0" name="historic" width="-1" type="field"/>
      <column hidden="0" name="land_area" width="-1" type="field"/>
      <column hidden="0" name="landuse" width="-1" type="field"/>
      <column hidden="0" name="leisure" width="-1" type="field"/>
      <column hidden="0" name="man_made" width="-1" type="field"/>
      <column hidden="0" name="military" width="-1" type="field"/>
      <column hidden="0" name="natural" width="-1" type="field"/>
      <column hidden="0" name="office" width="-1" type="field"/>
      <column hidden="0" name="place" width="-1" type="field"/>
      <column hidden="0" name="shop" width="-1" type="field"/>
      <column hidden="0" name="sport" width="-1" type="field"/>
      <column hidden="0" name="tourism" width="-1" type="field"/>
      <column hidden="0" name="other_tags" width="-1" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="admin_leve"/>
    <field editable="1" name="aeroway"/>
    <field editable="1" name="amenity"/>
    <field editable="1" name="barrier"/>
    <field editable="1" name="boundary"/>
    <field editable="1" name="building"/>
    <field editable="1" name="craft"/>
    <field editable="1" name="geological"/>
    <field editable="1" name="historic"/>
    <field editable="1" name="land_area"/>
    <field editable="1" name="landuse"/>
    <field editable="1" name="leisure"/>
    <field editable="1" name="man_made"/>
    <field editable="1" name="military"/>
    <field editable="1" name="name"/>
    <field editable="1" name="natural"/>
    <field editable="1" name="office"/>
    <field editable="1" name="osm_id"/>
    <field editable="1" name="osm_way_id"/>
    <field editable="1" name="other_tags"/>
    <field editable="1" name="place"/>
    <field editable="1" name="shop"/>
    <field editable="1" name="sport"/>
    <field editable="1" name="tourism"/>
    <field editable="1" name="type"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="admin_leve"/>
    <field labelOnTop="0" name="aeroway"/>
    <field labelOnTop="0" name="amenity"/>
    <field labelOnTop="0" name="barrier"/>
    <field labelOnTop="0" name="boundary"/>
    <field labelOnTop="0" name="building"/>
    <field labelOnTop="0" name="craft"/>
    <field labelOnTop="0" name="geological"/>
    <field labelOnTop="0" name="historic"/>
    <field labelOnTop="0" name="land_area"/>
    <field labelOnTop="0" name="landuse"/>
    <field labelOnTop="0" name="leisure"/>
    <field labelOnTop="0" name="man_made"/>
    <field labelOnTop="0" name="military"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="natural"/>
    <field labelOnTop="0" name="office"/>
    <field labelOnTop="0" name="osm_id"/>
    <field labelOnTop="0" name="osm_way_id"/>
    <field labelOnTop="0" name="other_tags"/>
    <field labelOnTop="0" name="place"/>
    <field labelOnTop="0" name="shop"/>
    <field labelOnTop="0" name="sport"/>
    <field labelOnTop="0" name="tourism"/>
    <field labelOnTop="0" name="type"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>name</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
