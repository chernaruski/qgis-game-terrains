from pathlib import Path

import zipfile
import sys
import configparser
import os


EXCLUDE = (
    '__pycache__',
    '_gsdata_',
    '.tools',
    '.cache',
    '.git',
    '.ipython',
    '.gitlab-ci',
    '.gitlab-ci.yml',
    '.vscode',
    'resources',
    'test',
    'tests',
    '.editorconfig',
    '.env',
    '.gitignore',
    '.gitlab-ci-release.yml',
    'gtt.code-workspace',
    'pylintrc',
    'NOTES.md',
    'todo.md',
)

def iterfiles(path: Path, top=False):
    """Recursive file iterator with exclude list"""

    if top:
        yield path

    for child in path.iterdir():
        child: Path
        if child.name not in EXCLUDE:
            if child.is_dir():
                yield child
                for file in iterfiles(child):
                    yield file
            else: 
                yield child


def bump_version(source: Path):
    """Updates the metadata file with release tag"""
    version = os.getenv('CI_COMMIT_TAG', '0.1')
    if version.startswith('v'):
        version = version[1:]

    file = source.parent / 'metadata.txt'
    config = configparser.ConfigParser()
    config.read(file)
    config['general']['version'] = version
    with file.open(mode='w') as fp:
        config.write(fp)


def create_zip(source: Path, filename='release'):
    target = source / 'assets' / f'{filename}.zip'
    target.parent.mkdir(parents=True, exist_ok=True)

    files = source.parent
    print(f"Writing file {target.name}")
    with zipfile.ZipFile(str(target), mode='w') as zf:
        for file in iterfiles(files, top=True):
            zf.write(str(file), str(filename / file), zipfile.ZIP_DEFLATED)
            
    print(f"Finished writing file")

if __name__ == "__main__":
    source = Path(__file__).parent
    bump_version(source)
    create_zip(source, filename=sys.argv[1])