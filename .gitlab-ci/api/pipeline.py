import requests
import json
import os

def load_vars():
    project_id = os.getenv('CI_PROJECT_ID', None)
    if project_id is None:
        raise Exception("CI_PROJECT_ID key isn't set, make sure load .environment.yml")

    api_key = os.getenv('GITLAB_API_KEY', "")
    session = requests.session()
    session.headers.update({"PRIVATE-TOKEN": api_key})

    return (project_id, api_key, session)

def clean_pipeline():
    project_id, api_key, session = load_vars()
    response = session.get(f"http://gitlab.com/api/v4/projects/{project_id}/pipelines")
    pipelines = [f['id'] for f in response.json()]

    ## REMOVE THE PIPELINES
    for pipeline in pipelines:
        url = f"http://gitlab.com/api/v4/projects/{project_id}/pipelines/{pipeline}"
        response = session.delete(url)
        print(response)

def clean_jobs():
    project_id, api_key, session = load_vars()
    response = session.get(f"http://gitlab.com/api/v4/projects/{project_id}/jobs")
    jobs = [f['id'] for f in response.json()]

    ## REMOVE THE PIPELINES
    for job in jobs:
        url = f"http://gitlab.com/api/v4/projects/{project_id}/jobs/{job}/erase"
        response = session.request(url)
        print(response)


def clean_releases():
    project_id, api_key, session = load_vars()
    response = session.get(f"http://gitlab.com/api/v4/projects/{project_id}/releases")
    releases = [f['tag_name'] for f in response.json()]

    ## REMOVE THE RELEASE
    for release in releases:
        url = f"http://gitlab.com/api/v4/projects/{project_id}/releases/{release}"
        response = session.delete(url)
        print(response)