import datetime
import os
from pathlib import Path

from api.release import run
from api.wiki import update_home, update_page


def update_wiki_release(files):
    print("## Updating wiki home page")
    print("Files:")
    print(files)
    with (Path(__file__).parents[1] / 'README.md').open() as fp:
        data = fp.read()

    now = datetime.datetime.now()
    time = str(now.strftime('%m-%d-%y %H:%M:%S'))
    data = data.replace('{{download}}', files)
    data = data.replace('{{time}}', time)
    update_home(data)


def update_wiki_version():
    version = os.getenv('CI_COMMIT_TAG', '0.1')
    if version.startswith('v'):
        version = version[1:]
    update_page('version', version)

if __name__ == "__main__":
    yaml_file = Path(__file__).parent / '.gitlab-ci-release.yml'
    success, result = run(yaml_file)
    if success:
        files = result['files']
        changelog = result['changelog']
        update_wiki_release(files)
        update_wiki_version()
        update_page('changelog', changelog)