import os
import time
import math

from PyQt5.QtCore import QDir, QFileInfo
from qgis.core import QgsApplication, QgsProject

def userFolder(target='gameterraintools'):
    """Gets user folder"""
    uuser_dir = os.path.join(QgsApplication.qgisSettingsDirPath(), target)
    if not QDir(uuser_dir).exists():
        QDir().mkpath(uuser_dir)

    return str(QDir.toNativeSeparators(uuser_dir))

def get_uid():
    t = time.time()
    m = math.floor(t)
    uid = '{:8x}{:05x}'.format(m, int((t - m) * 1000000))
    return uid